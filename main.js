var express = require("express");
var app=express();
app.use(express.static(__dirname + "/public"));
app.use("/libs", express.static(__dirname + "/bower_components"));

port=3000;

app.listen(port,function(){
    console.log("success");
})